const express = require('express');

const router = express.Router();


//======== Start Admin Controller =================//
const AdminController = require('../controllers/Admin/AdminController');

// admin  Validation 
const AdminLoginValidation = require('../middleware/validator/admin/login');

//======== Over Admin Controller =================//


//======== Start API Controller =================//
const UserController = require('../controllers/Api/UserController');
const NotificationController = require('../controllers/Api/NotificationController');
const ImageUploadController = require('../controllers/Api/ImageUploadController');
// const SettingController = require('../controllers/Api/SettingController');

//======== Over API Controller =================//


//======== start validation   =================//
const RegisterValidation = require('../middleware/validator/user/register');
const ProfileUpdateValidation = require('../middleware/validator/user/update');
const PasswordUpdateValidation = require('../middleware/validator/user/passwordUpdate');
const LoginValidation = require('../middleware/validator/user/login');


const authMiddleware = require('../middleware/UserAuth');
const AdminAuthMiddleware = require('../middleware/AdminAuth');
//======== End validation   =================//
router.post('/image-upload',  ImageUploadController.uploadImage);

//========Start API  Routes =================//
router.prefix('/admin', (route) => {
	// route.get('/get_setting',  SettingController.get);
	route.post('/login', AdminLoginValidation.login, AdminController.login);
	route.post('/logout', AdminAuthMiddleware , AdminController.logout);
	route.post('/profile-get', AdminAuthMiddleware , AdminController.profile);
	route.post('/profile-update', AdminAuthMiddleware , AdminController.update);
	route.post('/change-password', AdminAuthMiddleware , AdminController.changePassword);

});

router.prefix('/api', (route) => {
	route.post('/login', LoginValidation.login, UserController.login);
	route.get('/refresh-auth-token', authMiddleware , UserController.refreshAuthToken);
	route.post('/register', RegisterValidation.register, UserController.register);
	route.get('/logout', authMiddleware , UserController.logout);
	route.post('/social-check',  UserController.socialCheck);
	route.post('/verify-otp',  UserController.verifyOtp);
	route.post('/phone-register',  UserController.phoneRegister);
	route.get('/profile-get', authMiddleware , UserController.profile);
	route.post('/profile-update', authMiddleware , ProfileUpdateValidation.update ,  UserController.update);
	route.post('/reset-password', authMiddleware , PasswordUpdateValidation.update ,  UserController.passwordUpdate);
	route.post('/notification', NotificationController.send);
	route.post('/cryptoSend', NotificationController.cryptoSend);

});
//========End API  Routes =================//
module.exports = router;