var express = require('express');
const path = require('path');
const url = require('url');
var http = require('http');
const bodyParser = require("body-parser");
require('dotenv').config()
// Load required DB Model
const {
    sequelize,
} = require('./helpers/DBConnect');
const log4js = require("log4js");

log4js.configure({
    appenders: {
        everything: {
            type: 'dateFile',
            filename: './logger/all-the-logs.log',
            maxLogSize: 10485760,
            backups: 3,
            compress: true
        }
    },
    categories: {
        default: { appenders: ['everything'], level: 'debug' }
    }
});
// Router Prefix Setup 
express.application.prefix = express.Router.prefix = function (path, configure) {
    var router = express.Router();
    this.use(path, router);
    configure(router);
    return router;
};
// prefix Over

const routes = require('./routes');
const {DecryptedData} = require('./middleware/secure/DecryptedData');
const app = express();

// prefix start

// body mathi data get  karva start
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
// body mathi data get  karva over
const cors = require("cors");
const corsOptions = {
    origin: '*',
    credentials: true,            //access-control-allow-credentials:true
    optionSuccessStatus: 200,
}
app.use(express.static('public'));
app.use(DecryptedData);
app.use(cors(corsOptions)) // Use this after the variable declaration
// start route 

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.use('/', routes);
// over route 

// server create start
app.set('port', '4000');
var server = http.createServer(app);
server.listen('4000');
// server create over
