const encryptedDataResponse = (async (data) => {
    const crypto = require("crypto");
    const cipher = crypto.createCipheriv(process.env.algorithm, process.env.Securitykey, process.env.initVector);
    const message = JSON.stringify(data);
    let encryptedData = cipher.update(message, "utf-8", "hex");
    encryptedData += cipher.final("hex");
    var hmac = crypto.createHmac('sha256', process.env.Securitykey);
    data = hmac.update('nodejsera');
    gen_hmac = data.digest('hex');
    var response = {
        'mac': gen_hmac,
        'value': encryptedData
    }
    return response;
});

const encryptedData = (async (req, res, data) => {
    if (req.body.env) {
        if (req.body.env == 'test') {
            return JSON.stringify(data);
        } else {
            return await encryptedDataResponse(data);
        }
    } else {
        return await encryptedDataResponse(data);
    }
});

module.exports = {
    encryptedData,
}