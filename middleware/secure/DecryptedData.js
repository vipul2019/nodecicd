
const DecryptedDataResponse = (async (req , res , next) => {
    const crypto = require("crypto");
    const decipher = crypto.createDecipheriv(process.env.algorithm, process.env.Securitykey, process.env.initVector);
    let encryptedData;
    if(req.body.value){
        encryptedData = req.body.value;
    }
    let decryptedData = decipher.update(encryptedData, "hex", "utf-8");
    decryptedData += decipher.final("utf8");
    next();

});

const DecryptedData = (async (req, res, data , next ) => {
    if (req.body.env) {
        if (req.body.env == 'test') {
            return  JSON.stringify(data);
        }else{
            return DecryptedDataResponse(req , res , next );
        }
    }else{
        return DecryptedDataResponse(req , res , next );
    }
    
});

module.exports = {
    DecryptedData,
}


