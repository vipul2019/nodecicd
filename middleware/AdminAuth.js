const jwt = require('jsonwebtoken');
const {
    sendResponse,
    sendAuthError
} = require("../controllers/admin/baseController");
const { Admin } = require('../helpers/DBConnect');



module.exports = function (req, res, next) {
    // console.log(req.headers);
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    const isRefreshToken = req.headers['isrefreshtoken'];
    let resUnauthorized = {
        'message' : "Unauthorized Access!",
        "error": { 'Unauthorized': 'authorize token missing' }
    }
    if (token === undefined) {
        return sendAuthError(res, resUnauthorized);
    }

    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
    }
    if (token) {
        jwt.verify(token, process.env.secret, async (err, decoded) => {
            if (err) {
                if (err.name == "TokenExpiredError") {
                    if (isRefreshToken) {
                        const payload = jwt.verify(token, process.env.secret, { ignoreExpiration: true }, async (err, decodede) => {
                            const AdminInfo = await Admin.findOne({
                                where: { token: decodede.tokendata }
                            });
                            if(AdminInfo){
                                req.decoded = AdminInfo.dataValues;
                                next();
                            }
                        });
                    } else {                        
                        resUnauthorized = {
                            'message' : "Unauthorized Access!",
                            "error": { 
                                'Unauthorized': 'Unauthorized Access',
                                "isExpired": 1
                            }
                        }
                        return sendAuthError(res, resUnauthorized);
                    }
                } else {                   
                    return sendAuthError(res, resUnauthorized);
                }

            } else {

                const AdminInfo = await Admin.findOne({
                    where: { token: decoded.tokendata }
                });
                if (AdminInfo === null) {
                    return sendAuthError(res, resUnauthorized);

                } else {
                    if (isRefreshToken) {
                        resUnauthorized = {
                            'message' : "Unauthorized Access!",
                            "error": { 'token': 'Your Current Token is not Expired.' }
                        }
                        return sendAuthError(res, resUnauthorized);
                    }

                    if (AdminInfo.dataValues.status == 0) {
                        resUnauthorized = {
                            'message' : "Unauthorized Access!",
                            "error":  { 'account': 'Your Account has been blocked.' }
                        }
                        return sendAuthError(res, resUnauthorized);
                    } else {
                        req.decoded = AdminInfo.dataValues;
                        next();
                    }
                }
            }
        });
    } else {
        return sendAuthError(res, resUnauthorized);
    }
};