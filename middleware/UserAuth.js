const jwt = require('jsonwebtoken');
const {
    sendResponse,
    sendAuthError
} = require("../controllers/api/baseController");
const { User } = require('../helpers/DBConnect');



module.exports = function (req, res, next) {
    // console.log(req.headers);
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    const isRefreshToken = req.headers['isrefreshtoken'];
    let resUnauthorized = {
        'message' : "Unauthorized Access!",
        "error": { 'Unauthorized': 'authorize token missing' }
    }
    console.log(token);
    if (token === undefined) {
        return sendAuthError(res, resUnauthorized);
    }

    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
    }
    if (token) {
        jwt.verify(token, process.env.secret, async (err, decoded) => {
            if (err) {
                if (err.name == "TokenExpiredError") {
                    if (isRefreshToken) {
                        const payload = jwt.verify(token, process.env.secret, { ignoreExpiration: true }, async (err, decodede) => {
                            const userInfo = await User.findOne({
                                where: { token: decodede.tokendata }
                            });
                            if(userInfo){
                                req.decoded = userInfo.dataValues;
                                next();
                            }
                        });
                    } else {                        
                        resUnauthorized = {
                            'message' : "Unauthorized Access!",
                            "error": { 
                                'Unauthorized': 'Unauthorized Access',
                                "isExpired": 1
                            }
                        }
                        return sendAuthError(res, resUnauthorized);
                    }
                } else {                   
                    return sendAuthError(res, resUnauthorized);
                }

            } else {

                const userInfo = await User.findOne({
                    where: { token: decoded.tokendata }
                });
                if (userInfo === null) {
                    return sendAuthError(res, resUnauthorized);

                } else {
                    if (isRefreshToken) {
                        resUnauthorized = {
                            'message' : "Unauthorized Access!",
                            "error": { 'token': 'Your Current Token is not Expired.' }
                        }
                        return sendAuthError(res, resUnauthorized);
                    }

                    if (userInfo.dataValues.status == 0) {
                        resUnauthorized = {
                            'message' : "Unauthorized Access!",
                            "error":  { 'account': 'Your Account has been blocked.' }
                        }
                        return sendAuthError(res, resUnauthorized);
                    } else {
                        req.decoded = userInfo.dataValues;
                        next();
                    }
                }
            }
        });
    } else {
        return sendAuthError(res, resUnauthorized);
    }
};