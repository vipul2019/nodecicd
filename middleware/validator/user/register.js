const validator = require('../../../helpers/validate');
const {
    sendValidationError
} = require("../../../controllers/api/baseController");


const register = async (req, res, next) => {
    const validationRule = {
        "email": "required|email",
        "name": "required|string",
        "country_code": "required|string",
        // "email": "required|email|exist:User,email",
        // "name": "required|string|exist:User,name",
        "address": "required|string",
        "phone_no": "required|string",
        "password": "required|string|min:6|confirmed",
        "password_confirmation": "required|string|min:6",
        "device_token": "required",
        "device_type": "required",
    }
    try {
        await validator(req.body, validationRule, {}, (erros, status) => {
            if (!status) {
                sendValidationError(req,res, erros);
            } else {
                next();
            }
        });
    } catch (e) {
        console.log(e);
    }

}

module.exports = {
    register
}