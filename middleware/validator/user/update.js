const validator = require('../../../helpers/validate');
const {
    sendValidationError
} = require("../../../controllers/api/baseController");


const update = async (req, res, next) => {
    const validationRule = {
        "email": "required|email",
        "name": "required|string",
        "address": "required|string",
        "phone_no": "required|string",
        "date_of_birth": "required|string",
        "profile_pic": "required|string",
    }
    try {
        await validator(req.body, validationRule, {}, (erros, status) => {
            if (!status) {
                sendValidationError(req, res, erros);
            } else {
                next();
            }
        });
    } catch (e) {
        console.log(e);
    }

}

module.exports = {
    update
}