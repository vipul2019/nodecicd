const validator = require('../../../helpers/validate');
const {
    sendValidationError
} = require("../../../controllers/api/baseController");

const login = async (req, res, next) => {
    const validationRule = {
        "country_code": "required",
        "phone_no": "required",
        "password": "required|string|min:6",
        "device_token": "required",
        "device_type": "required",
    }
    try {
        await validator(req.body, validationRule, {}, (erros, status) => {
            if (!status) {
                sendValidationError(req , res, erros);
            } else {
                next();
            }
        });
    } catch (e) {
        console.log(e);
    }
}

module.exports = {
    login
}