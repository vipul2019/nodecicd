const validator = require('../../../helpers/validate');
const {
    sendValidationError
} = require("../../../controllers/api/baseController");


const update = async (req, res, next) => {
    const validationRule = {
        "country_code": "required|string",
        "phone_no": "required|string",
        "password": "required|string|min:6|confirmed",
        "password_confirmation": "required|string|min:6",
    }
    try {
        await validator(req.body, validationRule, {}, (erros, status) => {
            if (!status) {
                sendValidationError(req, res, erros);
            } else {
                next();
            }
        });
    } catch (e) {
        console.log(e);
    }

}

module.exports = {
    update
}