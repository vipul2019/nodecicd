const validator = require('../../../helpers/validate');
const {
    sendValidationError
} = require("../../../controllers/admin/baseController");

const login = async (req, res, next) => {
    const validationRule = {
        "email": "required|email",
        "password": "required|string|min:6",
    }
    try {
        const isValidate = await validator(req.body, validationRule, {}, (erros, status) => {
            if (!status) {
                sendValidationError(res, erros);
            } else {
                next();
            }
        });
    } catch (e) {
        console.log(e);
    }
}

module.exports = {
    login
}