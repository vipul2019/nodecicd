'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Settings', null, {});
    await queryInterface.bulkInsert('Settings', [
      {
        key: 'name',
        value: 'Als',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        key: 'email',
        value: 'admin@admin.com',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        key: 'image',
        value: '',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        key: 'db_backup_email_id',
        value: 'admin@admin.com',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
  ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Settings', null, {});

  }
};
