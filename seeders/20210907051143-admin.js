'use strict';
const bcrypt = require('bcrypt');
module.exports = {
    up: async (queryInterface, Sequelize) => {
        const saltRounds = 10;
        const myPlaintextPassword = "admin@123";
        const salt = bcrypt.genSaltSync(saltRounds);
        const hash = await bcrypt.hashSync(myPlaintextPassword, salt);
        await queryInterface.bulkInsert('Admins', [
            {
                name: 'Admin',
                email: 'admin@admin.com',
                password: hash,
                createdAt: new Date(),
                updatedAt: new Date(),
            }
        ], {});
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.bulkDelete('Admins', null, {});

    }
};
