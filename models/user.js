'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };

    User.init({
        name: DataTypes.STRING,
        facebook_id: DataTypes.STRING,
        google_id: DataTypes.STRING,
        social_type: DataTypes.STRING,
        apple_id: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        payment_password: DataTypes.STRING,
        address: DataTypes.STRING,
        date_of_birth: DataTypes.DATE,
        country_code: DataTypes.STRING,
        phone_no: DataTypes.STRING,
        status: DataTypes.INTEGER,
        user_verify: DataTypes.INTEGER,
        otp: DataTypes.STRING,
        profile_pic: {
            type: DataTypes.STRING,
            get() {
                const img_path = 'uploads/user/profile/';
                return process.env.APP_BASE_URL + img_path + this.getDataValue('profile_pic');
            },
        },
        status: DataTypes.STRING,
        token: DataTypes.STRING,
        remember_token: DataTypes.STRING,
        otp: DataTypes.STRING,
        device_token: DataTypes.STRING,        
        device_type: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'User',
    });
    return User;
};