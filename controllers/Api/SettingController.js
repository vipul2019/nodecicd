const express = require("express");
const bodyParser = require("body-parser");
const {
    sendSuccess,
} = require("../api/baseController");
const Settings = require("../../models/Settings");
const get = (async (req, res) => {
    let Settings = await Settings.all();
    return sendSuccess(res, Settings);
});

module.exports = {
    get,
}