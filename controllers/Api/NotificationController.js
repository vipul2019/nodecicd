const {
    sendSuccess,
} = require("../api/baseController");

const {
    sendNotification
} = require('../../helpers/CommonHelpers');

const cryptoSend = (async (req, res) => {
    var payload = {
        notification: {
            title: "Account Deposit",
            body: "A deposit to your savings account has just cleared."
        },
        data: {
            account: "Savings",
            balance: "$3020.25"
        }
    };

    return sendSuccess(req,res, payload);

});

const send = (async (req, res) => {
    const {
        device_token
    } = req.body;

    var admin = require("firebase-admin");
    var serviceAccount = require("./../../helpers/als-transfer-firebase-adminsdk-ma3nd-eeb11a7925.json");
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        // databaseURL: "https://console.firebase.google.com/u/0/project/als-transfer/overview"
    });
    var registrationToken = device_token;
    var payload = {
        notification: {
            title: "Account Deposit",
            body: "A deposit to your savings account has just cleared."
        },
        data: {
            account: "Savings",
            balance: "$3020.25"
        }
    };

    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    };
    await admin.messaging().sendToDevice(registrationToken, payload, options)
        .then(function (response) {
            console.log("Successfully sent message:", response);
        })
        .catch(function (error) {
            console.log("Error sending message:", error);
        });
    return sendSuccess(res, []);
});

module.exports = {
    send,
    cryptoSend,
}