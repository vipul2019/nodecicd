const express = require("express");
const bodyParser = require("body-parser");
const log4js = require("log4js");
const logger = log4js.getLogger();
const {
    sendSuccess,
    sendError
} = require("../api/baseController");

// // load model
const {
    User
} = require('../../helpers/DBConnect');
const {
    TokenFunction
} = require('../../helpers/CommonHelpers');

const md5 = require("md5");
const bcrypt = require('bcrypt');

const getData = (async (req, res) => {
    userInfo = await User.findOne({
        where: {
            id: req
        },
        attributes: [
            'id',
            'email',
            'device_token',
            'device_type',
            'name',
            'address',
            'country_code',
            'phone_no',
            'date_of_birth',
            'profile_pic',
        ]
    });
    return userInfo;
});
const login = (async (req, res) => {
    const {
        phone_no,
        country_code,
        password,
        device_token,
        device_type,
    } = req.body;
    const passwordSend = password;
    let userInfo = null;

    try {
        userInfo = await User.findOne({
            where: {
                phone_no: phone_no,
                country_code: country_code            
            }
        });
        if (userInfo) {
            await bcrypt.compare(passwordSend, userInfo.password, async function (err, result) {
                if (result == true) {
                    var dataTokenFunction = TokenFunction();
                    var tokendata = dataTokenFunction.tokendata;
                    var token = dataTokenFunction.token;
                    userInfo.token = tokendata;
                    userInfo.device_token = device_token;
                    userInfo.device_type = device_type;
                    await userInfo.save();
                    userInfo.access_token = token;

                    var dataReturn = await getData(userInfo.id);
                    let data = dataReturn.toJSON();
                    data['access_token'] = token;
                    return sendSuccess(req, res, data);
                } else {
                    resUnauthorized = {
                        'message': "oops , password does not match",
                    }
                    return sendError(req , res, resUnauthorized);
                }
            })
        }
        else {
            resUnauthorized = {
                'message': "User not found",
            }
            return sendError(req,res, resUnauthorized);
        }
    } catch (err) {
        logger.info("User login");
        logger.info(err);
        resUnauthorized = {
            'message': "Internal Server error",
        }
        return sendError(req , res, resUnauthorized);
    }

});
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}
const phoneRegister = (async (req, res) => {
    try {
        const [userInfo] = await User.findOrCreate({
            where: { 
                phone_no: phone_no,
                country_code: country_code
            },
        });
        var remember_token= makeid(55);
        var otp = Math.floor(1000 + Math.random() * 9000);
        userInfo.country_code = req.body.country_code;
        userInfo.phone_no = req.body.phone_no;
        userInfo.remember_token = remember_token;
        userInfo.otp = otp;
        userInfo.save();
        console.log(userInfo);
        let data = {};
        data['token_type'] = 'register';
        data['remember_token'] = remember_token;
        data['otp'] = otp;
        return sendSuccess(req,res, data);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Register User ");
        logger.info(err);
        return sendError(req,res, resUnauthorized);
    }
});
const register = (async (req, res) => {
    try {
        const saltRounds = 10;
        const myPlaintextPassword = req.body.password;
        const salt = bcrypt.genSaltSync(saltRounds);
        const hash = await bcrypt.hashSync(myPlaintextPassword, salt);
        var dataTokenFunction = TokenFunction();
        var tokendata = dataTokenFunction.tokendata;
        var token = dataTokenFunction.token;
        const [userInfo] = await User.findOrCreate({
            where: { 
                phone_no: req.body.phone_no,
                country_code: req.body.country_code,            
            },
            defaults: {
                facebook_id: req.body.facebook_id,
            }
        });
        userInfo.email = req.body.email;
        userInfo.country_code = req.body.country_code;
        userInfo.phone_no = req.body.phone_no;
        userInfo.name = req.body.name;
        userInfo.address = req.body.address;
        userInfo.device_type = req.body.device_type;
        userInfo.device_token = req.body.device_token;
        userInfo.facebook_id = req.body.facebook_id;
        userInfo.password = hash;
        userInfo.token = tokendata;
        userInfo.save();

        var dataReturn = await getData(userInfo.id);
        let data = dataReturn.toJSON();
        data['access_token'] = token;
        return sendSuccess(req,res, data);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Register User ");
        logger.info(err);
        return sendError(req , res, resUnauthorized);
    }
});
const logout = (async (req, res) => {
    try {
        userInfo = await User.findOne({
            where: {
                id: req.decoded.id
            },
        });
        userInfo.token = '';
        userInfo.device_token = '';
        await userInfo.save();

        return sendSuccess(req,res, {});
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Register Logout User ");
        logger.info(err);
        return sendError(req , res, resUnauthorized);
    }
});
const socialCheck = (async (req, res) => {
    try {
        const {
            auth_type,
            facebook_id,
            google_id,
            apple_id,
        } = req.body;
        var auth = '';
        let userInfo = [];

        if (auth_type == 1) {// facebook 
            auth = { facebook_id: facebook_id }
        }
        if (auth_type == 2) {// Google 
            auth = { google_id: google_id }
        }
        if (auth_type == 3) {// apple 
            auth = { apple_id: apple_id }
        }
        userInfo = await User.findOne({
            where: auth,
        });
        var dataResponse = {}
        if(userInfo){
            var dataTokenFunction = TokenFunction();
            var tokendata = dataTokenFunction.tokendata;
            var token = dataTokenFunction.token;
            userInfo.token = tokendata;
            userInfo.save();

            var dataReturn = await getData(userInfo.id);
            let data = dataReturn.toJSON();
            data['access_token'] = token;
            dataResponse  = data;
        }
        return sendSuccess(req,res, dataResponse);
        
        //   if(userInfo == null){
        //       userInfo = {};
        //   }
        // return sendSuccess(res,!userInfo ? {} : userInfo);
    } catch (err) {
        message = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Register socialCheck User ");
        logger.info(err);
        return sendError(req , res, message);
    }
});
const refreshAuthToken = (async (req, res) => {
    try {
        let olduser = req.decoded;
        var dataTokenFunction = TokenFunction();
        var tokendata = dataTokenFunction.tokendata;
        var token = dataTokenFunction.token;
        let userupdatdata = {
            token: tokendata,
        }
        await User.update(userupdatdata, {
            where: {
                id: olduser.id
            }
        });
        const responseData = {
            'message': "refresh Token successfully ",
            'data': token,
        };
        logger.info("User refresh  Login");
        logger.info(token);
        return sendSuccess(req ,res, responseData);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Register Refsresh token User ");
        logger.info(err);
        return sendError(req ,res, resUnauthorized);
    }
});
const verifyOtp = (async (req, res) => {
    try {
        const {
            otp,
            token,
        } = req.body;
        userInfo = await User.findOne({
            where: {
                remember_token: token
            }
        });
        if (userInfo) {
            if(userInfo.otp == otp){
                userInfo.otp = null;
                userInfo.save();
                return sendSuccess(req,res, {});

            }else{
                errorMessage = {
                    'message': "Invalid Otp! Please check otp again.",
                }
                return sendError(req ,res, errorMessage);
            }
        }
        else {
            errorMessage = {
                'message': "User not Found! Please register to continue.",
            }
            return sendError(req ,res, errorMessage);
        }
    } catch (err) {
        logger.info("User verify Otp");
        logger.info(err);
        errorMessage = {
            'message': "Internal Server error",
        }
        return sendError(req ,res, errorMessage);
    }

});
const profile = (async (req, res) => {
    try {
        dataInfo  = await getData( req.decoded.id );
        return sendSuccess(req,res, dataInfo);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("User Profile get");
        logger.info(err);
        return sendError(req,res, resUnauthorized);
    }
});
const update = (async (req, res) => {
    try {
        dataInfo = await User.findOne({
            where: {
                id: req.decoded.id
            },
        });
        const {
            name,
            email,
            phone_no,
            date_of_birth,
            country_code,
            profile_pic,
            address,
        } = req.body;

        dataInfo.name = name;
        dataInfo.country_code = country_code;
        dataInfo.phone_no = phone_no;
        dataInfo.email = email;
        dataInfo.date_of_birth = date_of_birth;
        dataInfo.address = address;
        dataInfo.profile_pic = profile_pic;
        dataInfo.save();
        responseData  = await getData( req.decoded.id );

        return sendSuccess(req,res, responseData);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Admin Profile update");
        logger.info(err);
        return sendError(req ,res, resUnauthorized);
    }
});
const passwordUpdate = (async (req, res) => {
    try {
        const {
            phone_no,
        } = req.body;

        const saltRounds = 10;
        const myPlaintextPassword = req.body.password;
        const salt = bcrypt.genSaltSync(saltRounds);
        const hash = await bcrypt.hashSync(myPlaintextPassword, salt);

        dataInfo = await User.findOne({
            where: {
                country_code: country_code,
                phone_no: phone_no
            },
        });
        dataInfo.password = hash;
        dataInfo.save();
        return sendSuccess(req ,res, {});
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Admin Profile update");
        logger.info(err);
        return sendError(req , res, resUnauthorized);
    }
});

module.exports = {
    register,
    login,
    logout,
    socialCheck,
    refreshAuthToken,
    phoneRegister,
    profile,
    update,
    passwordUpdate,
    verifyOtp,
}