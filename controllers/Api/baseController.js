
const {
    encryptedData,
} = require("../../middleware/secure/EncryptedData");

const sendResponse = (async (res, req) => {
    res.status(200).send(req);
});

const sendAuthError = (async (res, req) => {
    res.status(401).send(req.error)
});

const sendError = (async (req, res, error) => {
    let response = await encryptedData(req, res, error);
    res.status(422).send(response);
});
const sendValidationError = (async (req , res, error) => {
    let transformed = {};
    if (error) {
        Object.keys(error).forEach(function (key, val) {
            transformed[key] = error[key][0];
        });
    }
    let response = await encryptedData(req, res, transformed);
    res.status(422).send(response);
});

const sendSuccess = (async (req, res, data) => {
    let response = await encryptedData(req, res, data);
    res.status(200).send(response);
});

module.exports = {
    sendResponse,
    sendError,
    sendAuthError,
    sendSuccess,
    sendValidationError,
}