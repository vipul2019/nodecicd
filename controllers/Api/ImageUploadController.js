const express = require("express");
const bodyParser = require("body-parser");
const multer = require('multer');
const path = require('path');
const mkdirp = require('mkdirp');
const log4js = require("log4js");
const logger = log4js.getLogger();

const fs = require('fs');


const {
    sendSuccess,
    sendError,
} = require("../api/baseController");
const md5 = require("md5");

const uploadImage = (async (req, res) => {
    let filename;
    var imagePath = '';

    const storage = multer.diskStorage({
        destination: function (req, file, callback) {
            const {
                type
            } = req.body;
            if (type == 1) {
                imagePath = 'uploads/admin_profile/';
            } else if (type == 2) {
                imagePath = 'uploads/logo';
            } else {

            }
            // console.log(imagePath);
            // mkdirp(imagePath, err => callback(console.log(err), imagePath));
            // // console.log(imagePath);
            // // mkdirp(imagePath);
            // const made = mkdirp.sync(imagePath)
            // console.log(`made directories, starting with ${made}`)
            callback(null, './public/' + imagePath);

        },
        filename: function (req, file, callback) {
            callback(null, md5(Date.now()) + path.extname(file.originalname));
        }
    });
    const uploaFiles = multer({
        storage: storage,
        // fileFilter: imageFilter
    }).single('avatar');
    uploaFiles(req, res, async (err) => {
        if (!req.file) {
            resUnauthorized = {
                'message': "Please select an image !!",
            }
            logger.info("Image Upload ");
            logger.info(err);
            return sendError(res, resUnauthorized);

        } else if (err) {
            resUnauthorized = {
                'message': err,
            }
            logger.info("Image Upload ");
            logger.info(err);
            return sendError(res, resUnauthorized);
        } else {
            try {
                avatar = process.env.APP_BASE_URL + imagePath + req.file.filename;
                const responseData = {
                    'message': "Image Updated Successfully",
                    'data': {
                        img: avatar,
                        filename: req.file.filename
                    }

                };
                return sendSuccess(res, responseData);
            } catch (err) {
                resUnauthorized = {
                    'message': "oops ,Something Went Wrong !!",
                }
                logger.info("Image Upload ");
                logger.info(err);
                return sendError(res, resUnauthorized);

            }
        }
    });
});


module.exports = {
    uploadImage,
}