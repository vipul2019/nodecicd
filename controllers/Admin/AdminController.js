const express = require("express");
const bodyParser = require("body-parser");
const jwt = require('jsonwebtoken');
const log4js = require("log4js");
const logger = log4js.getLogger();

const {
    sendSuccess,
    sendError
} = require("../admin/baseController");
const {
    TokenFunction
} = require('../../helpers/CommonHelpers');
function adminTokenFunction() {
    let tokendata = md5(Math.floor(Math.random() * 9000000000) + 1000000000) + md5(new Date(new Date().toUTCString()));
    let token = jwt.sign({
        tokendata
    }, process.env.secret, {
        expiresIn: '24h'
    });
    const dataResponse = {
        token: token,
        tokendata: tokendata,
    }
    return dataResponse;
}

// // load model
const {
    Admin
} = require('../../helpers/DBConnect');

const md5 = require("md5");
const bcrypt = require('bcrypt');

const login = (async (req, res) => {
    try {
        const {
            email,
            password
        } = req.body;
        const saltRounds = 10;
        const passwordSend = password;
        let resUnauthorized;

        let AdminResult = await Admin.findOne({
            where: {
                email: email
            }
        }).then(function (result) {
            return result;
        });
        if (AdminResult) {
            let loginAdmin = await bcrypt.compare(passwordSend, AdminResult.password, (async function (err, result) {
                if (result == true) {
                    var dataTokenFunction = adminTokenFunction();
                    var tokendata = dataTokenFunction.tokendata;
                    var token = dataTokenFunction.token;
                    AdminResult.token = tokendata;
                    AdminResult.save();
                    let AdminResultResponse = await Admin.findOne({
                        where: { email: email }
                    }).then(function (result) {
                        return result;
                    });
                    let data = AdminResultResponse.toJSON();
                    data['access_token'] = token;
                    const responseData = {
                        'message': "get successfully ",
                        'data': data,
                    };
                    logger.info("Admin Login");
                    logger.info(responseData);
                    return sendSuccess(res, responseData);
                } else {
                    resUnauthorized = {
                        'message': "oops , password does not match",
                    }
                    return sendError(res, resUnauthorized);
                }
            }));
        } else {
            resUnauthorized = {
                'message': "oops , email is does not exist",
            }
            return sendError(res, resUnauthorized);
        }
    } catch (err) {
        resUnauthorized = {
            'message': err,
        }
        return sendError(res, resUnauthorized);
    }
});

const logout = (async (req, res) => {
    try {
        dataInfo = await Admin.findOne({
            where: {
                id: req.decoded.id
            },
        });
        dataInfo.token = '';
        await dataInfo.save();
        const responseData = {
            'message': "Admin logout successfully ",
            'data': [],
        };
        return sendSuccess(res, responseData);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Register Logout Admin");
        logger.info(err);
        return sendError(res, resUnauthorized);
    }
});
const profile = (async (req, res) => {
    try {
        dataInfo = await Admin.findOne({
            where: {
                id: req.decoded.id
            },
        });
        const responseData = {
            'message': "Admin Profile get successfully ",
            'data': dataInfo,
        };
        return sendSuccess(res, responseData);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Admin Profile get");
        logger.info(err);
        return sendError(res, resUnauthorized);
    }
});
const update = (async (req, res) => {
    try {
        dataInfo = await Admin.findOne({
            where: {
                id: req.decoded.id
            },
        });
        const {
            name,
            email,
            profile_pic,
        } = req.body;

        dataInfo.name = name;
        dataInfo.email = email;
        dataInfo.profile_pic = profile_pic;
        dataInfo.save();
        // updtae here admin data 
        const responseData = {
            'message': "Profile update successfully ",
            'data': dataInfo,
        };
        return sendSuccess(res, responseData);
    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Admin Profile update");
        logger.info(err);
        return sendError(res, resUnauthorized);
    }
});
const changePassword = (async (req, res) => {
    try {
        dataInfo = await Admin.findOne({
            where: {
                id: req.decoded.id
            },
        });

        const passwordSend = req.body.oldPassword;


        await bcrypt.compare(passwordSend, dataInfo.password, (async function (err, result) {
            console.log(result);
            if (result == true) {                
                const saltRounds = 10;
                const myPlaintextPassword = req.body.password;
                const salt = bcrypt.genSaltSync(saltRounds);
                const hash = await bcrypt.hashSync(myPlaintextPassword, salt);
                dataInfo.password = hash;
                dataInfo.save();
                
        const responseData = {
            'message': "password update successfully ",
            'data': dataInfo,
        };
        return sendSuccess(res, responseData);

            } else {
                resUnauthorized = {
                    'message': "oops , Password Wrong",
                }
                return sendError(res, resUnauthorized);
            }
        }));

    } catch (err) {
        resUnauthorized = {
            'message': "oops ,Something Went Wrong !!",
        }
        logger.info("Admin password update");
        logger.info(err);
        return sendError(res, resUnauthorized);
    }
});




module.exports = {
    login,
    logout,
    profile,
    update,
    changePassword,
}