const sendResponse = (async (res, req) => {
    const responseData = {
        'message': req.message,
        'data': req.data
    };
    res.status(200).send(responseData);
});

const sendAuthError = (async (res, req) => {
    res.status(401).send(req.error)
});

const sendError = (async (res, error) => {
    res.status(422).send(error)
});
const sendValidationError = (async (res, error) => {
    let transformed = { };
    if (error) {
        Object.keys(error).forEach(function (key, val) {
            transformed[key] = error[key][0];
        });
    }
    res.status(422).send({ status: false, error: transformed })
});

const sendSuccess = (async (res, req) => {
    const responseData = {
        'message': req.message,
        'data': req.data
    };
    res.status(200).send(responseData);
});


module.exports = {
    sendResponse,
    sendError,
    sendAuthError,
    sendSuccess,
    sendValidationError,
}