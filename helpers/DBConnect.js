const Sequelize = require('sequelize')
const UserModel = require('./../models/user')
const AdminModel = require('./../models/admin')
const SettingModel = require('./../models/setting')

const dotenv = require('dotenv');
dotenv.config();

const sequelize = new Sequelize(process.env.db, process.env.user, process.env.password, {
	host: "127.0.0.1",
	dialect: 'mysql',
	logging: false,
	pool: {
		max: 10,
		min: 0,
		acquire: 30000,
		idle: 10000
	}
})

const User = UserModel(sequelize, Sequelize)
const Admin = AdminModel(sequelize, Sequelize)
const Setting = SettingModel(sequelize, Sequelize)

module.exports = {
	sequelize,
	User,
	Setting,
	Admin
}