const jwt = require('jsonwebtoken');
const md5 = require("md5");
const bcrypt = require('bcrypt');



function TokenFunction() {
    let tokendata = md5(Math.floor(Math.random() * 9000000000) + 1000000000) + md5(new Date(new Date().toUTCString()));
    let token = jwt.sign({
        tokendata
    }, process.env.secret, {
        expiresIn: '1m'
    });
    const dataResponse = {
        token: token,
        tokendata: tokendata,
    }
    return dataResponse;
}

module.exports = {
    TokenFunction,
}