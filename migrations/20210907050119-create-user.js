'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('Users', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            facebook_id: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            google_id: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            social_type: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            apple_id: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            email: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            password: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            payment_password: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            address: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            date_of_birth: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            country_code: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            phone_no: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            status: {
                type: Sequelize.INTEGER,
                comment: "1 is active 0 is deactive ",
                defaultValue: '1'
            },
            user_verify: {
                type: Sequelize.INTEGER,
                comment: "1 is active 0 is deactive ",
                defaultValue: '0'
            },
            token: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            otp: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            device_token: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            device_type: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            remember_token: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            profile_pic: {
                type: Sequelize.STRING
              },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Users');
    }
};